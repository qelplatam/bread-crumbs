# name: bread-crumbs
# about: Overrided Category Bread Crumb
# version: 0.0.3
# authors: Sergio Marreiro

register_asset "javascripts/discourse/app/templates/components/bread-crumbs.hbs";
register_asset "javascripts/initializers/bread-crumbs.js.es6";
register_asset "stylesheets/common/bread-crumbs.scss";

