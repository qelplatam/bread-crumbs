import { getOwner } from "discourse-common/lib/get-owner";
import { withPluginApi } from 'discourse/lib/plugin-api';
import { default as BreadCrumbs } from 'discourse/components/bread-crumbs';
import discourseComputed from "discourse-common/utils/decorators";


function initializePlugin(api)
{
    BreadCrumbs.reopen({
      actions: {
        scrollMe(event){
          console.log(event);
          let elem = document.querySelector(".overrided-category-boxes")
          let scroll = event=="left" ? -316 : 316;
          elem.scrollBy({
            top: 0,
            left: scroll,
            behavior: 'smooth'
          });      
        },
      },      
        @discourseComputed("category")
        hidden(category) {
          let url = window.location.pathname; 
          return !!category || url=="/categories";
        },
    })
}

export default
{
  name: 'bread-crumbs',

  initialize(container)
  {
    withPluginApi('0.1', api => initializePlugin(api));
  }
};